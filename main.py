import png
import pandas
import os

HEIGHT = 28
WIDTH = 28
LABEL_COLUMN = 0
FIRST_PIXEL = 1
LAST_PIXEL = 785

LETTERS = {
    0: "A",
    1: "B",
    2: "C",
    3: "D",
    4: "E",
    5: "F",
    6: "G",
    7: "H",
    8: "I",
    10: "K",
    11: "L",
    12: "M",
    13: "N",
    14: "O",
    15: "P",
    16: "Q",
    17: "R",
    18: "S",
    19: "T",
    20: "U",
    21: "V",
    22: "W",
    23: "X",
    24: "Y",
}

if __name__ == '__main__':
    if not os.path.exists('hands'):
        os.makedirs('hands')
        for value in LETTERS.values():
            if not os.path.exists('hands\\' + value):
                os.makedirs('hands\\' + value)

    data = pandas.read_csv("path_to_learn_data")

    length = len(data.index)
    for csv_row in range(1):
        print("Hand " + str(csv_row + 1) + " of " + str(length))
        letter_code = data.values.item(LAST_PIXEL * csv_row)
        hand_array = [[] for _ in range(HEIGHT)]
        for csv_column in range(FIRST_PIXEL, LAST_PIXEL):
            hand_array[(csv_column - FIRST_PIXEL) // WIDTH].append(data.values.item(LAST_PIXEL * csv_row + csv_column))
        png.from_array(hand_array, 'L').save("hands\\" + LETTERS.get(letter_code) + "\\" + str(csv_row) + ".png")
