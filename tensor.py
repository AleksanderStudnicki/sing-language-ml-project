import matplotlib.pyplot as plt
import numpy as np
import PIL
import tensorflow as tf
import os
import pathlib
import glob

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential

HEIGHT = 28
WIDTH = 28
DATA_DIR = pathlib.Path("hands")

train_ds = tf.keras.preprocessing.image_dataset_from_directory(
    DATA_DIR,
    validation_split=0.2,
    subset="training",
    seed=123,
    image_size=(HEIGHT, WIDTH))

val_ds = tf.keras.preprocessing.image_dataset_from_directory(
    DATA_DIR,
    validation_split=0.2,
    subset="validation",
    seed=123,
    image_size=(HEIGHT, WIDTH))

class_names = train_ds.class_names
print(class_names)

plt.figure(figsize=(10, 10))
for images, labels in train_ds.take(1):
    for i in range(9):
        ax = plt.subplot(3, 3, i + 1)
        plt.imshow(images[i].numpy().astype("uint8"))
        plt.title(class_names[labels[i]])
        plt.axis("off")
        plt.show()

for image_batch, labels_batch in train_ds:
    print(image_batch.shape)
    print(labels_batch.shape)
    break

AUTOTUNE = tf.data.experimental.AUTOTUNE

train_ds = train_ds.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)

# normalization_layer = layers.experimental.preprocessing.Rescaling(1. / 255)
#
# normalized_ds = train_ds.map(lambda x, y: (normalization_layer(x), y))
# image_batch, labels_batch = next(iter(normalized_ds))
# first_image = image_batch[0]
# # Notice the pixels values are now in `[0,1]`.
# print(np.min(first_image), np.max(first_image))

num_classes = 24

# model = Sequential(
#     [
#         layers.experimental.preprocessing.Rescaling(1. / 255, input_shape=(HEIGHT, WIDTH, 3)),
#         layers.Conv2D(16, 3, padding='same', activation='relu'),
#         layers.MaxPooling2D(),
#         layers.Conv2D(32, 3, padding='same', activation='relu'),
#         layers.MaxPooling2D(),
#         layers.Conv2D(64, 3, padding='same', activation='relu'),
#         layers.MaxPooling2D(),
#         # layers.Conv2D(128, 3, padding='same', activation='relu'),
#         # layers.MaxPooling2D(),
#         layers.Flatten(),
#         layers.Dense(128, activation='relu'),
#         layers.Dense(num_classes)
#     ])
#
# data_augmentation = Sequential(
#     [
#         layers.experimental.preprocessing.RandomFlip("horizontal",
#                                                      input_shape=(HEIGHT,
#                                                                   WIDTH,
#                                                                   3)),
#         layers.experimental.preprocessing.RandomFlip("vertical",
#                                                      input_shape=(HEIGHT,
#                                                                   WIDTH,
#                                                                   3)),
#         layers.experimental.preprocessing.RandomRotation(0.1),
#     ]
# )

model = Sequential([
    layers.experimental.preprocessing.RandomFlip("horizontal",
                                                 input_shape=(HEIGHT,
                                                              WIDTH,
                                                              3)),
    layers.experimental.preprocessing.RandomFlip("vertical",
                                                 input_shape=(HEIGHT,
                                                              WIDTH,
                                                              3)),
    layers.experimental.preprocessing.RandomRotation(0.1),
    layers.experimental.preprocessing.Rescaling(1. / 255),
    layers.Conv2D(16, kernel_size=(3, 3), padding='same', activation='relu'),
    layers.MaxPooling2D(),
    layers.Conv2D(32, kernel_size=(3, 3), padding='same', activation='relu'),
    layers.MaxPooling2D(),
    layers.Conv2D(64, kernel_size=(3, 3), padding='same', activation='relu'),
    layers.MaxPooling2D(),
    # layers.Conv2D(128, 3, padding='same', activation='relu'),
    # layers.MaxPooling2D(),
    layers.Dropout(0.2),
    layers.Flatten(),
    layers.Dense(128, activation='relu'),
    layers.Dense(num_classes)
])

# model = Sequential([
#   data_augmentation,
#   layers.experimental.preprocessing.Rescaling(1./255),
#   layers.Conv2D(16, 3, padding='same', activation='relu'),
#   layers.MaxPooling2D(),
#   layers.Conv2D(32, 3, padding='same', activation='relu'),
#   layers.MaxPooling2D(),
#   layers.Conv2D(64, 3, padding='same', activation='relu'),
#   layers.MaxPooling2D(),
#   layers.Dropout(0.2),
#   layers.Flatten(),
#   layers.Dense(128, activation='relu'),
#   layers.Dense(num_classes)
# ])

model.compile(optimizer='rmsprop',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

model.summary()

epochs = 50
history = model.fit(
    train_ds,
    validation_data=val_ds,
    epochs=epochs
)

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(epochs)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
plt.show()

files = glob.glob("C:\\Users\\Aleksander\\PycharmProjects\\tf-test\\test-data\\*.png")
# files = glob.glob("C:\\Users\\Aleksander\\PycharmProjects\\tf-test\\my-test\\*.png")

total = len(files)
tries = 1000
good = 0

for i in range(tries):
    file = files[i]
    img = keras.preprocessing.image.load_img(
        file, target_size=(HEIGHT, WIDTH)
    )
    img_array = keras.preprocessing.image.img_to_array(img)
    img_array = tf.expand_dims(img_array, 0)

    predictions = model.predict(img_array)
    score = tf.nn.softmax(predictions[0])

    letter = file[:-4]
    letter = letter.split("-")[3]

    if letter == class_names[np.argmax(score)]:
        good = good + 1

    print(
        "{} - {} - This image most likely belongs to {} with a {:.2f} percent confidence."
            .format(i, letter, class_names[np.argmax(score)], 100 * np.max(score))
    )

print("CORRECT {} of {}".format(good, tries))
